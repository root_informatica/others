#!/bin/sh

## play online radio  with mplayer ##
# radioplayer.sh by @root_informatica.

TARGET=$1
# radio list.
R_LIST="$HOME/.radio"
# foregroound color.
FG="#a0a0a0"

# define the menu according to the environment.
[ -t 0 ] && MENU="fzy" \
	   || MENU="xmenu.sh"

[ -n "$TARGET" ] && \
    radio=$TARGET \
	|| TARGET=$($MENU < $R_LIST | cut -d '|' -f2)

# play it
if [ -n "$TARGET" ]; then
    [ -t 0 ] && mpv --no-video \
		    $TARGET \
	    || mpv --quiet \
		   --force-window=yes \
		   --lavfi-complex="[aid1]asplit[ao][a1];[a1]showwaves=mode=cline:colors=$FG:rate=25,format=rgb0 [vo]" \
		   $TARGET
fi

# ffmpeg-filters showwaves
# --lavfi-complex='[aid1]asplit[ao][a1];[a1]showwaves=mode=cline:colors=white:rate=25,format=rgb0 [vo]' \

# .radio file example:
# radio name | https://some-radio-url
