#!/bin/sh

## fzf menu for non-X sessions ##
# fzfmenu.sh by @root_informatica.

fzf --bind 'ctrl-e:print-query' --no-sort -m < /proc/$$/fd/0 > /proc/$$/fd/1

 
