#!/bin/sh

## this fetch was born from the search for a simpler way to get system info. Several lines and ways of doing are the product of the exchange of ideas with other users who share my convictions ##
# As more minimalist alternatives are also sfetch.sh and rootfetch.sh.
# microfetch.sh by @root.informatica.

# os name.
OS=$(awk -F '"' '/PRETTY_NAME/ {print $2}' /etc/os-release)
# kernel and arch.
read -r KERNEL < /proc/sys/kernel/osrelease
ARCH=$(getconf LONG_BIT)
# machine name.
read -r HOSTNAME < /etc/hostname
# user name (It's just a matter of making a "echo $USER").
# uptime.
UP=$(uptime -p)

# init.
# INIT=$(ps --no-headers -o comm 1)
# INIT=$(/sbin/init --version)
[ "$(ps --no-headers -o comm 1)" = "runit" ] && \
     INIT="runit" \
 || INIT=$(/sbin/init --version)
 
# cpu and gpu procesors.
CPU=$(awk -F ":" 'NR==5 {print $2}' /proc/cpuinfo | tr -s ' ')
GPU=$(lspci 2>/dev/null | awk -F ":" '/VGA/ {print $3}' | cut -c 1-50)

# check display for screensize and working environment.
if [ -n "$DISPLAY" ]; then
    SCREEN=$(sed 's/,/x/' < /sys/class/graphics/fb0/virtual_size)
    [ -n "$DESKTOP_SESSION" ] && \
        WE="$DESKTOP_SESSION" \
            || WE=$(xprop -root WM_NAME | cut -d '"' -f2)
else
    SCREEN=$(stty size | awk '{print $1 "rows " $2 "columns"}')
    tty=$(tty)
    WE=tty${tty##*/}
fi

# display server.
if [ -n "$DISPLAY" ]; then
    ps -e | grep -e 'wayland\|Xorg' > /dev/null && \
	D_SERVER="(Xorg)" \
	    || D_SERVER="(Wayland)"
fi

# memory.
RAM=$(awk '/MemTotal/ {total=$2} \
    /MemFree/ {free=$2} \
    /Buffers/ {buffers=$2} $1 ~ \
    /^Cached/ {cached=$2} END \
    {printf "%.0f\n",(total - (free + buffers + cached))/1024}' /proc/meminfo)
SWAP=$(awk '/SwapFree/ {s_free=$2} \
	   /SwapTotal/ {s_total=$2} END \
	   {printf "%.0f\n",(s_total - s_free)/1024}' /proc/meminfo)

# packages.
PKG=$([ -f /usr/bin/xbps-install ] && xbps-query -l | wc -l \
	|| dpkg --list | wc --lines)

# terminal env and shell.
TERM_ENV=$(printf '%s' "$TERM")
SHELL=$(printf '%s' "${SHELL##*/}")

# print formated information.
printf "
\033[1;37m [M] \033[30machine / ${HOSTNAME} / --user/ $USER / --up/ ${UP} / \033[0m
\033[1;37m [S] \033[30mystem / ${OS} / --arch/ ${ARCH} / \033[0m
\033[1;37m [K] \033[30mernel / ${KERNEL} / \033[0m
\033[1;37m [I] \033[30mnit / ${INIT} / \033[0m
\033[1;37m [P] \033[30mrocessor / ${CPU} / \033[0m
\033[1;37m [G] \033[30mraphics / ${GPU} / \033[0m
\033[1;37m [M] \033[30memory / ${RAM}Mib / --swap/ ${SWAP}Mib / \033[0m
\033[1;37m [P] \033[30mackages / ${PKG} / \033[0m
\033[1;37m [W] \033[30morkplace / ${WE} ${D_SERVER} / --screen/ ${SCREEN} / \033[0m
\033[1;37m [T] \033[30merm env / ${TERM_ENV} / \033[0m
\033[1;37m [S] \033[30mhell / ${SHELL} / \033[0m

"
# \033[1;37m OS: \033[30m
# \033[3;7;37m ${HOSTNAME} \033[0m
