#!/bin/sh

## this fetch was born from the search for a simpler way to get system info. Several lines and ways of doing are the product of the exchange of ideas with other users who share my convictions ##
# rootfetch.sh by @root.informatica.

# os name.
OS=$(awk -F '"' '/PRETTY_NAME/ {print $2}' /etc/os-release)
# kernel and arch.
read -r KERNEL < /proc/sys/kernel/osrelease
ARCH=$(getconf LONG_BIT)
# machine name.
read -r HOSTNAME < /etc/hostname

# init.
# INIT=$(ps --no-headers -o comm 1)
# INIT=$(/sbin/init --version)
[ "$(ps --no-headers -o comm 1)" = "runit" ] && \
    INIT="runit" \
	|| INIT=$(/sbin/init --version)

# cpu and gpu procesors.
CPU=$(awk -F ":" 'NR==5 {print $2}' /proc/cpuinfo | tr -s ' ')
# GPU=$(lspci 2>/dev/null | awk -F ":" '/VGA/ {print $3}' | cut -c 1-50)
GPU=$(lspci 2>/dev/null | awk -F ":" '/VGA/ {print $3}' | cut -c 1-47)

# check display for screensize and working environment.
if [ -n "$DISPLAY" ]; then
    SCREEN=$(sed 's/,/x/' < /sys/class/graphics/fb0/virtual_size)
    [ -n "$DESKTOP_SESSION" ] && \
	WE="$DESKTOP_SESSION" \
	    || WE=$(xprop -root WM_NAME | cut -d '"' -f2)
else
    SCREEN=$(stty size | awk '{print $1 "rows " $2 "columns"}')
    tty=$(tty)
    WE=tty${tty##*/}
fi

# display server.
if [ -n "$DISPLAY" ]; then
    ps -e | grep -e 'wayland\|Xorg' > /dev/null && \
	D_SERVER="(Xorg)" \
	    || D_SERVER="(Wayland)"
fi

# memory.
RAM=$(awk '/MemTotal/ {total=$2} \
    /MemFree/ {free=$2} \
    /Buffers/ {buffers=$2} $1 ~ \
    /^Cached/ {cached=$2} END \
    {printf "%.0f\n",(total - (free + buffers + cached))/1024}' /proc/meminfo)
SWAP=$(awk '/SwapFree/ {s_free=$2} \
	   /SwapTotal/ {s_total=$2} END \
	   {printf "%.0f\n",(s_total - s_free)/1024}' /proc/meminfo)

# packages.
PKG=$([ -f /usr/bin/xbps-install ] && xbps-query -l | wc -l \
	|| dpkg --list | wc --lines)

# terminal env and shell.
TERM_ENV=$(printf '%s' "$TERM")
SHELL=$(printf '%s' "${SHELL##*/}")

# define space.
space() {
    printf '\n'
}

# define top decoration.
above() {
    tput smacs
    printf '\033[0;33m%s\033[0m' " " "l" 
    printf '\033[0;33mq%.0s\033[0m' $(seq 1 6)
    tput rmacs
    printf '%s\033[3;7;31m%s\033[0m' " " " ${HOSTNAME} " " "
    tput smacs
    printf '\033[0;33mq%.0s\033[0m' $(seq 1 50) 
    tput rmacs
}

# define bottom decoration.
below() {
    tput smacs
    printf '\033[0;33m%s\033[0m' " " "m"
    printf '\033[0;33mq%.0s\033[0m' $(seq 1 50)
    tput rmacs
}

space
above

# print formated information.
printf "
  \033[1;37m OS: \033[30m ..................\033[0m \033[3;37m  ${OS} \033[0m
  \033[1;37m Kernel: \033[30m ..............\033[0m \033[3;37m  ${KERNEL}-${ARCH} \033[0m
  \033[1;37m Init: \033[30m ................\033[0m \033[3;37m  ${INIT} \033[0m
  \033[1;37m Processor: \033[30m ...........\033[0m \033[3;37m ${CPU} \033[0m
  \033[1;37m Graphics:\033[30m .............\033[0m \033[3;37m ${GPU} \033[0m
  \033[1;37m Mem: \033[30m .................\033[0m \033[3;37m  ${RAM}Mib ${SWAP}Mib\033[0m
  \033[1;37m Packages: \033[30m ............\033[0m \033[3;37m  ${PKG} \033[0m
  \033[1;37m Workplace: \033[30m ...........\033[0m \033[3;37m  ${WE} ${D_SERVER} ${SCREEN}\033[0m
  \033[1;37m Term Env: \033[30m ............\033[0m \033[3;37m  ${TERM_ENV} \033[0m
  \033[1;37m Shell: \033[30m ...............\033[0m \033[3;37m  ${SHELL} \033[0m
"
below
space 
