#!/bin/sh

## replace spaces in file and directory names with a "_" ##
# spacekiller.sh by @root_informatica.

usage() {
    cat<<EOF
######################
#### CAUTION!! #####
###################
Running this script will replace any existing spaces in file and folder names within the current directory with a _.
Make sure you are in the correct directory!
Only then write "proceed".
If you are not sure what you are doing, type "quit" to abort the script.
Use this script at your own risk!

proced = rename files.
quit = exit.

DIRECTORY: $(pwd)

entry:
EOF
}

space_kill() {
    # sed 
    for i in *; do
	mv "$i" $(echo "$i" | sed -e 's/ /_/g')
    done
}

clear
usage 

while read IN; do
    case $IN in
    proced)
	space_kill && \
	    exit 0
	   ;;
    quit)
	exit 0
	;;
    *)
	clear
	echo "Wrong entry!";
	sleep 1;
	clear
	usage
	;;
    esac
done

	   
# tr
#  for f in *; do mv "$f" `echo $f | tr ' ' '_' `; done
