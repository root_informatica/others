#!/bin/sh

## open web browser with the content of x selection ##
# xselgo.sh by @root_informatica.

# browser and selection. 
WEB_BROWSER="links -g"
X_SEL=$(xclip -selection clipboard -o)

# check if the selection is a url. If it is, open it. If not, search for results on the internet. 
printf "$X_SEL" | grep -Eq "^https://|^http://|^www." && $WEB_BROWSER $X_SEL \
	|| $WEB_BROWSER "https://duckduckgo.com/?q=$X_SEL"

## xselection ##
# under xprop: XSEL=$(xprop -root | awk -F "=" '/CUT_BUFFER0/ {gsub("\"","",$2); print $2}')
# under wmutils: XSEL=$(atomx CUT_BUFFER0 $(lsw -r))
# with xclip: XSEL=$(xclip -o)
