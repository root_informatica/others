#!/bin/sh

## some info width dzen2 ##
# wms_panel.sh by @root_informatica.

FLAG=$1
# personal mark.
P_MARK="$HOME/.message"
# beautiful art.
ART="
    ▄    ▄▄▄▄▄▄▄    ▄  
   ▀▀▄ ▄█████████▄ ▄▀▀  
       ██ ▀███▀ ██      
     ▄ ▀████▀████▀ ▄    
   ▀█    ██▀█▀██    █▀  

     █   █ █▄ ▄█ ▄▀▀    
     ▀▄▀▄▀ █ ▀ █ ▄██

"
# foreground and background colors.
FG="#404040"
BG="#101010"
# root-window width and heigth.
RW="1280"
RH="800"

clock() {
    while true; do
	date +%H:%M
	sleep 60
    done
}

# kill dzen
DZ_PID=$(pgrep -x dzen2)
if [ -n "$DZ_PID" ]; then
    pkill dzen2
    pkill dzpanel.sh
fi &

case $FLAG in
    -c)    # make a simple clock.
	w=360
	h=120
	x=$(((RW - w) / 2))
	y=$(((RH - h) / 2))
	clock | dzen2 -fn Hack-80 \
		      -fg "$FG" -bg "$BG" \
		      -w $w -h $h -x $x -y $y \
	    &
	    ;;
    -h)    # hello message.
	w=310
	h=40
	x=$(((RW - w) / 2))
	y=$(((RH - h) / 2))
	cat $P_MARK  | dzen2 -fn Hack-18 \
			    -e 'onstart=uncollapse' -l 2 \
			    -fg "$FG" -bg "$BG" \
			    -w $w -x $x -y $y -p \
	    &
	    ;;
    -s)    # make a simple status.
	w=500
	h=120
	x=$(((RW - w) / 2))
	y=$(((RH - h) / 2))
	status.sh | dzen2 -fn Hack-12 \
			  -e 'onstart=uncollapse' -l 6 \
	    		  -fg "$FG" -bg "$BG" \
	    		  -w $w -x $x -y $y -p \
	    &
	    ;;
    -v)    # valume value.
	w=420
	h=80
	x=$(((RW - w) / 2))
	y=$(((RH - h) / 2))
	vol=$(amixer sget Master | awk -F "[][]" '/Playback/ {print $2}' | tr -d '\n')
	echo "volume: $vol" | dzen2 -fn Hack-40 \
				    -fg "$FG" -bg "$BG" \
				    -w $w -h $h -x $x -y $y -p 1 \
	    &
	    ;;
    -w)    # beautiful art.
	w=370
	h=340
	x=$(((RW - w) / 2))
	y=$(((RH - h) / 2))
	echo "$ART" | dzen2 -fn Hack-18 \
			   -e 'onstart=uncollapse' -l 10 \
			   -fg "$FG" -bg "$BG"  \
			   -w $w -x $x -y $y -p \
	    &
	    ;;
    *)
	exit 0
	;;
esac
