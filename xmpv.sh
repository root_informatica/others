#!/bin/sh

## launch mpv together with a graphic session ##
# xmpv.sh by @root_informatica

INPUT=$1
MENU="fzy"

if [ -n "$INPUT" ]; then
    sx /usr/bin/mpv "$INPUT"
else
    sx /usr/bin/mpv "$(find . -maxdepth 1 \
    -type f \
    | grep -E '.avi|.mkv|.webm|.mp4|' \
    | $MENU)"
fi

