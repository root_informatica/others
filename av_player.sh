#!/bin/sh

## play video with or without X server (Xorg) ##
# av_player.hs by @root_informatica.

TARGET=$1

# check display. 
[ -n "$DISPLAY" ] && mpv "$TARGET" \
			    || mpv --vo=drm "$TARGET"

