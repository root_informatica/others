#!/bin/sh

## utility for previewing content in fzf ##
## based on vi-nuke from @Visone_Selektah.
# fzfpreview.sh by @root_informatica.

TARGET=$1
# target name.
T_NAME="${TARGET##*/}"

# extension.
ext="${T_NAME##*.}"
if [ -n "$ext" ]; then
    ext="$(printf "%s" "${ext}" | tr '[:upper:]' '[:lower:]')"
fi

    case "${ext}"  in
        ## Archive
        a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|\
        rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zst|zip|rar|7z)
                cat  "$TARGET"
                exit 0
		;;
        ## Audio
        aac|flac|m4a|mid|midi|mpa|mp2|mp3|ogg|wav|wma)
            	ffprobe "$TARGET"
            	exit 0
		;;
        ## Video
        avi|mkv|mp4|gif)
            ffprobe "$TARGET"
            exit 0
	    ;;
        ## Image
        png|jpg|jpeg|PNG|JPG|JPEG|svg)
            	chafa -c full -s 40x40 "$TARGET"
              exit 0
		;;
        ## log files
        log)
            	less "$TARGET"
            	exit 0
		;;
        ## plain text
        *)
                cat "$TARGET"
                exit 0
		;;
    esac
