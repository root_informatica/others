#!/bin/sh
## This script remains unfinished. It is supposed to upload, download, remove and basic manage remote and local files to and from nextcloud. ##
# nextcurl.sh by @root_informatica

# # nextcloud url, username, password and local dir.
. $HOME/.config/nextcloud/nextcloud_data 

FLAG=$1
REMOTE=$2
LOCAL=$3

help() {
    cat<<EOF

    Hello stupid! let me explain to you how to do some magic...

    syntax: --------------------------------------------------------------------------
    	  nextcurl.sh  <flag> <path_to_remote_file/folder> <name_of_the_download/upload_file>
	  --------------------------------------------------------------------------
    options:
       	  -u	updoad file.
    	  -d 	download file.
    	  -c 	create folder.
    	  -r    delete folder.
    	  -l    list files in folder.
	  -L	list folders.
	  -m    mass download.
    	  -h    print help.
	  -----------------------------------------------------------------------
EOF
}

download() {
    curl -X GET -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USER_NAME/$REMOTE --output $LOCAL
}

upload() {
    curl -X PUT -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USER_NAME/$REMOTE -T $LOCAL
}

create_folder() {
    curl -X MKCOL -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USER_NAME/$REMOTE
}

delete_folder() {
    curl -X DELETE -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USER_NAME/$REMOTE
}

list_files() {
    curl -X PROPFIND -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USER_NAME/$REMOTE \
	| sed 's/<\//\n/g' \
	| grep $USER_NAME \
	| sed "s/${USER_NAME}/ /g" \
	| cut -d ' ' -f 2
}

list_folders() {
    curl -X PROPFIND -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USER_NAME \
	| sed 's/<\//\n/g' \
	| grep $USER_NAME \
	| sed "s/${USER_NAME}/ /g" \
	| cut -d ' ' -f 2
}

mass_download() {
    for f in $(curl -X PROPFIND -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USERN/$REMOTE \
		   | sed 's/<\//\n/g' \
		   | grep $USER_NAME \
		   | sed "s/${USER_NAME}/ /g" \
		   | cut -d ' ' -f 2); do
	curl -X GET -u $USER_NAME:$PASS $URL/remote.php/dav/files/$USER_NAME/$f --output $(basename $f)
    done
}

    case $1 in
    -d)
	download
	;;
    -u)
	upload
	;;
    -c)
	create_folder
	;;
    -r)
	delete_folder
	;;
    -l)
	list_files
	;;
    -L)
	list_folders
	;;
    -m)
	mass_download
	;;												 
    -h)
	help
	;;
esac

