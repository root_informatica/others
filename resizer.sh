#!/bin/sh

## resize images with ImageMagick ##
# screen resolution based on fb device. Allows you to run the script even without xorg running,
# but is useless if the device is disabled.
# resize.sh by @root_informatica.

TARGETS=$@
# screen resolution based on fb device.
RES=$(sed 's/,/x/' < /sys/class/graphics/fb0/virtual_size)

[ -n "$TARGETS" ] && \
    for f in $TARGETS; do
	# determine file type.
	file_type=$(file --mime-type -b $f | cut -d '/' -f2)
	case $file_type in
	    png|jpg|jpeg) # more formats can be added here.
		# determine the size of the image to be formatted.
		image_size=$(identify -ping -format '%wx%h' $f)
		# discard images whose dimensions match the screen resolution.
		if [ "$image_size" != "$RES" ]; then
		    echo -e "\n[trying] -> $f ..."
		    # The command may differ depending on versions of ImageMagick.
		    magick $f -resize "!$RES" $f && \
			echo -e "[done] -> $f \n" \
			    || echo "[error!] -> $f"
		else
		    echo "[already!] -> $f $RES"
		fi
		;;
	    *)
		echo "[mistaken!] -> $f"
		;;
	esac
    done \
	|| echo "no targets!"
