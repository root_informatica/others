#!/bin/sh

## download adblock list for luakit ##
# adblocklist.sh by @root_informatica.

# lists urls
R_LISTS="https://easylist.to/easylist/easylist.txt \
https://easylist.to/easylist/easyprivacy.txt \
https://secure.fanboy.co.nz/fanboy-cookiemonster.txt"

# local directory lists
L_DIR="$HOME/.local/share/luakit/adblock"

# download
for l in $R_LISTS; do
    curl --url $l --output $L_DIR/${l##*/}
done

