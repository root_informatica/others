#!/bin/sh

## manage session and some more things ##
# session.sh by @root_informatica.

[ -t 0 ] && MENU="fzy" \
	|| MENU="xmenu.sh"

# keybindings reload.
KB_RELOAD="pkill -usr1 -x sxhkd"
# lock screen monitor program.
LOCK_PROGRAM="slock"
# poweroff monitor.
M_OFF="xset dpms force off"
# suspend to ram.
SUSPEND="doas zzz -z"
# hibernate.
HIBER="doas zzz -Z"
# reboot.
REBOOT="doas shutdown -r now"
# poweroff.
P_OFF="doas shutdown -h now"
# exit session.
EXIT="pkill wew"

# litle menu.
PROMPT="reload-key\nlock\nmonitor-off\nhalt\nreboot\nsuspend\nhibernate\nexit"

option=`echo -e $PROMPT | $MENU`
if [ ${#option} -gt 0 ]; then
    case $option in
	reload-key)
	    $KB_RELOAD
	    ;;
	lock)
	    $LOCK_PROGRAM
	    ;;
	monitor-off)
	    $M_OFF
	    ;;
	suspend)
	    $SUSPEND
	    ;;
	hibernate)
	    $HIBER
	    ;;
	halt)
            $P_OFF
	    ;;
	reboot)
            $REBOOT
            ;;
	exit)
            $EXIT
	    ;;
	*)
	  ;;
      esac

else
    exit 0
fi
