#!/bin/sh

## create qr codes from any url in terminal with image file option as output ##
## qr.sh by @root_informatica ##
# it depends on libqrencode service

URL="$1"
Q_RNAME="$2"

usage() {
    cat<<EOF 
usage: qr.sh <url>
EOF
}

if [ -n "$URL" ] && [ -n "$Q_RNAME" ]; then
    curl qrenco.de/$URL > /tmp/qr.txt
    convert -size 290x290 \
	    xc:black \
	    -font "Hack-Bold" \
	    -fill white \
	    -pointsize 16 \
	    -annotate -30-20 "@/tmp/qr.txt" $Q_RNAME.png
else
    usage
fi
