#!/bin/sh

DIRS="appimage
 binary
 document
 download
 chrdir
 git
 image
 music
 nextcloud
 source
 video"

DOTS="profile
shinit
Xresources
fbtermrc
bookmarks
emojis
message
signature"

# ckek current dir.
[ $(pwd) != $HOME ] && \
    echo "current dir -> $(pwd)!" \
	 && exit 0

# check dots.
echo "
## checking dots ................................
"

for f in $DOTS; do
    if [ -f ".$f" ]; then
	echo ".$f -> already esixt!"
    else
	ln -s $HOME/git/dots/$f $HOME/".$f"
	echo ".$f -> linked!"
    fi
done

# check dirs.
echo "
## checking dirs .................................
"

# create new dirs.
    for d in $DIRS; do
	if [ -d $d ]; then
	    echo "$d -> already exist!"
	else
	    mkdir $d
	    echo "$d -> created!"
	fi
    done

# rename existing dirs.
for d in $(ls); do
	 [ -d $d ] && \
	     case $d in
		 Descargas)
		     mv $d download
		     echo "$d renamed -> download"
		     ;;
		 Documentos)
		     mv $d document
		     echo "$d renamed -> document"
		     ;;
		 Imágenes)
		     mv $d image
		     echo "$d renamed -> image"
		     ;;
		 Música)
		     mv $d music
		     echo "$d renamed -> music"
		     ;;
		 Vídeos)
		     mv $d video
		     echo "$d renamed -> video"
		     ;;
	     esac
done
