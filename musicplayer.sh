#!/bin/sh

## play music with mplayer ##
# musicplayer.sh by @root_informatica.

# music dir.
M_DIR="$HOME/music"
# playlist path.
P_LIST="/tmp/playlist"
# foreground color.
FG="#a0a0a0"

# define the menu according to the environment.
[ -t 0 ] && MENU="fzy" \
	   || MENU="xmenu.sh"

# make the playlist.
album=$(find $M_DIR -type d | cut -d '/' -f 5- | $MENU)
[ -n "$album" ] && \
    find $M_DIR/$album -type f \
	| grep -E ".mp3|.opus|.flac|.webm|.ogg|.wav" > $P_LIST \
	       || exit 0

# play it.
if [ -f "$P_LIST" ]; then
    [ -t 0 ] && mpv --no-video \
		    --playlist=$P_LIST \
	    || mpv --quiet \
		   --force-window=yes \
		   --lavfi-complex="[aid1]asplit[ao][a1];[a1]showwaves=mode=cline:colors=$FG:rate=25,format=rgb0 [vo]" \
		   --playlist=$P_LIST
    rm $P_LIST
fi

# ffmpeg-filters showwaves
# --lavfi-complex='[aid1]asplit[ao][a1];[a1]showwaves=mode=cline:colors=white:rate=25,format=rgb0 [vo]' \
