#!/bin/sh

## images and bragging with your pretty desktop ##
# pfw is used to determine the window in focus. can be replaced by xprop or xdotool. 
# WARNING: The convert command is deprecated in IMv7, use "magick" instead of "convert" or "magick convert"
# screenshot.sh by @root_informatica.

# xmenu.
MENU="xmenu.sh"
# wmutils binary. (It can also be done with xdotool)
W_TARGET=$(pfw)
# date. 
DATE=`date +%d-%m-%Y_%H:%M:%S`
# screensht path.
S_PATH="$HOME/image/screenshot"
# legend to insert in the screnshots.
SIGNATURE="#root_informatica"
# editor program.
E_PROGRAM="gimp"
# time interval, just to improve performance.
DELAY="sleep 1"

# options
OPT=$(echo -e "desktop\nwindow\nselection\nmultiselect\nfancy\nedit\nshare" \
	  | $MENU)

# capture the desktop
desktop() {
    import -w root $S_PATH/screenshot_$DATE.png
}

# capture the desktop with signature
fancy() {
    fancy_temp_path="/tmp/screenshot_fancy.png"
    import -w root $fancy_temp_path
    magick $fancy_temp_path \
	    -gravity South \
	    -font DejaVu-Sans-Bold-Oblique \
	    -background transparent \
	    -fill 'rgba(255,255,255,0.35)' \
	    -pointsize 50 \
	    -annotate +380+4 "$SIGNATURE" $S_PATH/screenshot_$DATE.png;
    rm $fancy_temp_path
}

# capture focused window
window() {
    import -w $W_TARGET $S_PATH/screenshot_$DATE.png
}

# select region
selection() {
    import $S_PATH/screenshot_$DATE.png
}

# select multiple regions
multi_selection() {
    multi_shot_path="/tmp/screenshot_multi.png"
    import -snaps $(echo -e "2\n3\n4\n5\n6" | $MENU) $multi_shot_path;
    magick convert -background black \
	   -append /tmp/screenshot_multi-*.png $S_PATH/selection_compose_$DATE.png;
    rm /tmp/screenshot_multi-*.png
}

# edit after capturing the desktop
edit() {
    edit_shot_path="/tmp/screenshot_edit.png"
    import -w root $edit_shot_path
    $E_PROGRAM $edit_shot_path;
    rm $edit_shot_path
}

# share desktop [ https://0x0.st ]
share() {
    share_shot_path="/tmp/screenshot_share.png"
    import $share_shot_path
    curl -F "file=@$share_shot_path" -F "secret=" https://0x0.st | xclip
    xclip -o | xclip -sel c;
    rm $share_shot_path
}

case $OPT in
    desktop)
	$DELAY
	desktop
	;;
    fancy)
	$DELAY
     	fancy
        ;;
    window)
	window
        ;;
    selection)
        selection
        ;;
    multiselect)
	multi_selection
	;;
    edit)
	$DELAY
	edit 
	;;
    share)
	share
	;;
    quit)    # exit
	exit 0
        ;;
esac
