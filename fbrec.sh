#!/bin/sh

## extract video from framebuffer device ##
## fbrec.sh by @root_informatica.

# date mark.
DATE=$(date +%d-%m-%Y_%H:%M:%S)
# framebuffer device.
FB="/dev/fb0"
# output dir.
OUT_DIR="$HOME"

ffmpeg -f fbdev -framerate 25 -i $FB $OUT_DIR/$DATE.mp4
