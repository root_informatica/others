#!/bin/sh

## capture desktop content in video ##
# capturemedia.sh by @root_informatica.

# menu.
XMENU=xmenu.sh
# timestamp.
DATE=$(date +%m-%d-%Y_%H:%M:%S)
# record pid.
R_PID=$(cat /tmp/record.pid)

# webcam in a frame.
webcam() {
    ffplay -f video4linux2 \
    	   -i /dev/video0 \
	   -video_size 420x340 \
	   -fflags nobuffer & echo $! > /tmp/record.pid
}

# record mic.
mic() {
    arecord record_audio_$DAT.mp3 & echo $! > /tmp/record.pid
}

# record desktop (smartphone compatibility).
desktop() {
    ffmpeg -f x11grab \
	   -s $(sed 's/,/x/' < /sys/class/graphics/fb0/virtual_size) \
	   -r 12 -i :1.0+0,0 \
	   -vcodec libx264 record_video_$DATE.mp4 & echo $! > /tmp/record.pid
}

# record desktop with audio from the michrophone.
desktop_mic() {
    ffmpeg -video_size $(sed 's/,/x/' < /sys/class/graphics/fb0/virtual_size) \
	   -framerate 25 \
	   -f x11grab \
	   -i :1 \
	   -f alsa \
	   -ac 2 \
	   -i hw:0 record_audiovideo.$DATE.mp4 & echo $! > /tmp/record.pid
}

# add audio to an already recorded video.
add_audio() {
    # music dir.
    m_dir="$HOME/Mùsica"
    # video target.
    video_target=$(find $HOME -maxdepth 1 -type f | grep ".mp4" | $XMENU)
    # audio target
    audio_target=$(find $m_dir -type f | $XMENU)
    # mix command.
    ffmpeg -i $video_target -stream_loop -1 -i $audio_target -c copy -shortest -map 0:v:0 -map 1:a:0 -vcodec libx264 -vf format=yuv420p -acodec aac -ab 128k -ac 2 -ar 44100  record_addaudio_$DATE.mp4
}

stop() {
    kill $R_PID
}

OPT=$(echo -e "addaudio\nmic\ndesktop\ndesktop_mic\nwebcam\nstop" | $XMENU)
case $OPT in
    addaudio)
	add_audio
	;;
    mic)
	mic
	;;
    desktop)
	desktop
	;;
    desktop_mic)
	desktop_mic
	;;
    webcam)
        webcam
	;;
    stop)
	# kill'em all.
	stop
	;;
esac
