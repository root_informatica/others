#!/bin/sh

## Download new content from your favorite YouTube channels ##
# youtube_sync.sh by @root_informatica.

# download program path.
DL="$HOME/.local/bin/yt-dlp"
# downloads history file path.
H_PATH="$HOME/.config/ytpd/ytpd_history"
# batch file path.
B_PATH="$HOME/.config/ytpd/ytpd_batch"
# podcast dir path.
P_DIR="$HOME/download/ytpd"
# yt-dlp pid.
YTDLP_PID=$(pgrep -x yt-dlp)

# check for proces
if [ ! "$YTDLP_PID" ]; then

    # start yt-dlp
    $DL --download-archive $H_PATH -x --audio-format vorbis --audio-quality 0 -i --batch-file $B_PATH --output "$P_DIR/%(upload_date)s_%(channel)s_%(duration)s_%(title)s.%(ext)s"

fi



