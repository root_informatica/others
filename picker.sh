#!/bin/sh

## crappy script to browse and open files in different scenarios ##
## depends on rooter.sh ##
# open.sh by @root_informatica.

[ -t 0 ] && MENU="fzy" \
	|| MENU="xmenu.sh"

while true; do
    # file/directory target.
    target=$(ls | $MENU)
    # if target is a directory.
    if [ -d "$target" ]; then
	# move to it (Obviously if target is ".." it goes back a directory)
	cd "$target"

    else    # if target is a file
	# rooter.sh will try to open it.
	rooter.sh "$target"
	break
    fi
done


	
