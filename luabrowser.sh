#!/bin/sh

## overly sophisticated launcher for luakit ##
# The -t flag requires the TOR service running 
# luabrowser.sh by @root_informatica.

INPUT=$@
# tor pid.
T_PID=$(pgrep -x tor)

# export some luakit variables.
export WEBKIT_DISABLE_COMPOSITING_MODE=1

[ -n "$INPUT" ] && \
    case $INPUT in
	-t)   # torsock browser (verify that the tor service is running)
	    # check tor pid.
	    [ -n "$T_PID" ] && \
		torsocks luakit -u https://check.torproject.org/ --nounique \
		    || exit 0
	    ;;
	*)   # default browser
	    luakit "$INPUT"
	    ;;
    esac \
	|| luakit


