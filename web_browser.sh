#!/bin/sh

## open url with or without X server (Xorg); ##
# web_browser.sh by @root_informatica.

TARGET=$1

# check display.
[ -n "$DISPLAY" ] && luabrowser.sh "$TARGET" \
	|| links "$TARGET"
