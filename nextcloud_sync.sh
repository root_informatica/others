#!/bin/bash

## sync with nextcloud ##
# highly recommended to use a device password for nextcloud.
# I have started using rclone instead of this script.
# nextcloud_sync.sh by @root_informatica.

# nextcloud url, username, password and local dir.
. $HOME/.config/nextcloud/nextcloud_data 

# some decoration output.
echo "================= nextcloud_sync ====================="
echo "start at $(date +%H:%Mhs)"
echo -e "\n"

# let's get the job done.
nextcloudcmd -u $USER -p $PASS $L_DIR $URL &&
    echo "nextcloud_root sync done at $(date +%H:%Mhs)"
    echo "nextcloud_sync.sh finish!"
    
