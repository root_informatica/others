#!/bin/sh

## set an image as wallpaper / put wallpaper in root-window ##
# !ImageMagick. Version 7 and later change the "convert" command to "magick".
# wallpaper.sh by @root_informatica.

# image path.
I_PATH=$1
# wallpaper path.
W_PATH="$HOME/.wall.png"
# screen resolution.
RES=$(sed 's/,/x/' < /sys/class/graphics/fb0/virtual_size)

# put the existing wallpaper in root-window.
set_wall() {
    [ -f "$W_PATH" ] && \
	display -resize "!$RES" -window root $W_PATH
}

# create wallpaper from an image.
make_wall() {
    [ -f "$I_PATH" ] &&
    magick $I_PATH -resize "!$RES" $W_PATH
}
    
# let's proceed
[ -n "$I_PATH" ] && \
    make_wall && set_wall \
	|| set_wall

