#!/bin/sh

## mount and umount partitions a little less than "by hand". ##
# usblinkd.sh by @root_informatica.

# list removable usb devices.
removable_list() {
    usb_keys=($(
		 grep -Hv ^0$ /sys/block/*/removable |
		     sed s/removable:.*$/device\\/uevent/ |
		     xargs grep -H ^DRIVER=sd |
		     sed s/device.uevent.*$/size/ |
		     xargs grep -Hv ^0$ |
		     cut -d / -f 4
	     ))
    
    for dev in ${usb_keys[@]} ; do
	echo $dev \"$(sed -e s/\ *$//g </sys/block/$dev/device/model)\"
    done
}

# partitions in.
device_target=$(removable_list | fzy | cut -c 1-3)
for p in $(lsblk -l -n -o NAME /dev/$device_target | grep "[[:alnum:]]\{4\}"); do
    mount_check=$(grep -w "$p"  < /proc/mounts | cut -d ' ' -f2)
    if [ -n "$mount_check" ]; then
	doas umount /dev/$p
	rm -r /tmp/$p
	echo "the volume $p was removed and de /tmp/$p directory deleted."
	
    else
	if [ -d "/tmp/$p" ]; then
	    doas mount /dev/$p /tmp/$p
	    echo "the /tmp/$p directory already existed and the volume was mounted on it!"
	    echo "$p mounted on /tmp/$p"
	else
	    mkdir /tmp/$p
	    doas mount /dev/$p /tmp/$p
	    echo "$p mounted on /tmp/$p"
	fi
    fi
done
 
