#!/bin/sh

## view images with or without X server (Xorg); ##
## inside or outside a terminal. (requires sixels graphics support) ##
# watch.sh by @root_informatica.

TARGET=$1

# check display.
[ -n "$DISPLAY" ] && nsxiv "$TARGET" \
	|| fbv -f "$TARGET"

# sixel graphics.
# convert  $TARGET  -geometry 800x480  sixel:-

