#!/bin/sh

## simple status ##
# status.sh by @root_informatica.

# network.
network() {
    lan_state=$(cat /sys/class/net/enp*/operstate)
    wlan_state=$(cat /sys/class/net/wlp*/operstate)
    
    echo "lan/ $lan_state / --wifi/ $wlan_state /"
}

# mails.
mail() {
    COUNT_1=$(ls ~/.mail/disroot/INBOX/new | wc -l)
    COUNT_2=$(ls ~/.mail/cock/INBOX/new | wc -l)

    echo "@disroot.org/ $COUNT_1 / --@cocaine.ninja/ $COUNT_2 /"
}

# battery.
battery() {
    read -r STATUS < /sys/class/power_supply/BAT0/status
    read -r LEVEL < /sys/class/power_supply/BAT0/capacity 

    if [ "$STATUS" = "Charging" ] || [ "$STATUS" = "Full" ]; then
        echo "charging/ +$LEVEL% /"
    else
        echo "discharging/ -$LEVEL% /"
    fi
}

# memory.
memory() {
    mem_total=$(awk '/MemTotal/ {print $2/1024}' /proc/meminfo)
    mem_available=$(awk '/MemAvailable/ {print $2/1024}' /proc/meminfo)
    swap_total=$(awk '/SwapTotal/ {print $2/1024}' /proc/meminfo)
    swap_free=$(awk '/SwapFree/ {print $2/1024}' /proc/meminfo)
    ram=$(echo "(($mem_total-$mem_available)*100)/$mem_total" | bc)
    swap=$(echo "(($swap_total-$swap_free)*100)/$swap_total" | bc)

    echo "ram/ $ram% / --swp/ $swap% /"
}

# podcast.
podcast() {
    podcast_dir="$HOME/download/ytpd"
    if [ -d "$podcast_dir" ]; then
       podcast_total=$(ls $podcast_dir | grep -v trash | wc -l)
       podcast_weight=$(du -h $podcast_dir | awk '{print $1}')
       echo  "podcast/ $podcast_total / --weight/ $podcast_weight /"
    else
        echo "/ no podcast dir /"
    fi
}

## output ##
cat<<EOF

 [B] attery: $(battery)
 [N] etwork: $(network)
 [M] emory: $(memory)
 [M] ails: $(mail)
 [P] odcast: $(podcast)

EOF
