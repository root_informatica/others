#!/bin/sh

##  Emacs launcher. check if we are running inside a terminal emulator or not and run -t or -c flag. ##
# emax.sh by @root_informatica

[ -t 0 ] && emacsclient -t --alternate-editor="" \
	|| emacsclient -c --alternate-editor=""
	

