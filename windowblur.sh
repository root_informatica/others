#!/bin/sh

## blur the content of a window to hide it ## 
# windowblur.sh by @root_informatica.

# date and time.
DATE=$(date +%m-%d-%Y_%H:%M:%S)
# window id. (wmutils)
W_TARGET=$(pfw)
# window geometry. (wmutils)
GEO=$(wattr wh $(pfw) | sed 's/ /x/g')

# capture focused window. 
import -w $W_TARGET /tmp/windowblur_$DATE.png
convert /tmp/windowblur_$DATE.png -gaussian-blur 8x8 /tmp/windowblur_blur_$DATE.png
# brur it.
nsxiv -b -g $GEO -e $W_TARGET /tmp/windowblur_blur_$DATE.png

#### WID and GEO widh "xdotool" ####
# WID=$(xdotool getwindowfocus)
# GEO=$(xdotool getwindowfocus getwindowgeometry | awk -F " " '/Geometry/ {print $2}'
