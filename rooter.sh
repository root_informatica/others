#!/bin/sh

## a kind of Swiss army knife that will try to select the appropriate program to open the file or url that is passed to it ##
# rooter.sh is largely based on vi-nuke from visone. https://visone.codeberg.page/.
# rooter.sh by @root_informatica.

# input.
TARGET=$1

# regex to check if the input string is a url or not. 
is_url='(https?|http?|ftp|file)://[-[:alnum:]\+&@#/%?=~_|!:,.;]+'
if [[ $TARGET =~ $is_url ]]; then
    # clear file name.
    filename=${TARGET##*/}
    [ ! -f /tmp/$filename ] && \
	# download it.
	curl -s -o /tmp/$filename $TARGET
    # set new target.
    target=/tmp/$filename
else
    target=$1
fi

# view images with or without X server (Xorg).
image_viewer() {
    # check display.
    [ -n "$DISPLAY" ] && nsxiv "$1" \
	    || fbv -f "$1"
}

# read pdf's with or without X server (Xorg).
document_reader() {
    [ -n "$DISPLAY" ] && zathura "$1" \
	    || fbpdf "$1"
}

# play video with or without X server (Xorg).
av_player() {
    [ -n "$DISPLAY" ] && mpv "$1" \
	    || mpv --vo=drm "$1"
}

web_browser() {
    [ -n "$DISPLAY" ] && luabrowser.sh "$1" \
	    || links "$1"
}

# clear file ext. 
ext="${target##*.}"
if [ -n "$ext" ]; then
    ext="$(printf "%s" "${ext}" | tr '[:upper:]' '[:lower:]')"
fi

    case "${ext}"  in
        ## Archive
        a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|\
        rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zst|zip|rar|7z)
            extract "$target"
            exit 0
	    ;;

        ## PDF
        pdf)
	    document_reader "$target"
            exit 1
	    ;;

        ## Audio
        aac|flac|m4a|mid|midi|mpa|mp2|mp3|ogg|wav|wma|oga)
            av_player "$target"
            exit 1
	    ;;

        ## Video
        avi|mkv|mp4|webm|gif)
            av_player "$target"
            exit 1
	    ;;
        
	## Image
        png|jpg|jpeg|PNG|JPG|JPEG|svg)
            image_viewer "$target"
            exit 1
	    ;;

        ## Log files
        log)
	    emacsclientt -t --alternate-editor="" "$target"
	    exit 0
	    ;;

        ## BitTorrent
        torrenti|magnet)
            rtorrent "$target"
            exit 0
	    ;;

        ## OpenDocument
        odt|ods|odp|sxw)
	    luabrowser.sh "$target"
            exit 0
	    ;;

        ## Markdown
        md)
            emacsclient -t --alternate-editor="" "$target"
            exit 0
	    ;;

        ## HTML
        htm|html|xhtml)
            web_browser "$target"
            exit 0
	    ;;

        ## JSON
        json)
            jq --color-output . "$target"
            exit 0
	    ;;
	## scripts and text
	bash|txt|py|sh)
	    emacsclient -t --alternate-editor="" "$target"
	    exit 0
	    ;;

    esac
