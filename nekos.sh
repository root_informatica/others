#!/bin/sh

## pets on your desk ##
# shuf is not POSIX
# it depends on oneko
# nekos.sh by @root_informatica

# nekos.
N=$(shuf -e tora dog bsd sakura tomoyo -n 1)
# cloor.
C=$(shuf -e white red blue green yellow cyan magenta -n 1)
# speed.
S=$(shuf -i 1-20 -n 1)

oneko -$N -fg black -bg $C -speed $S
