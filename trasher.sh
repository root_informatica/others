#!/bin/sh

# file/s to delete. 
TARGET=$@
# trash dir.
T_DIR="$HOME/.trash"
# menu.
MENU="fzy"
# history path.
HISTORY_PATH="$HOME/.trash/.history"

# check if the trash dir exist.
[ ! -d $T_DIR ] && \
    mkdir $T_DIR && \
    echo "directorio $T_DIR creado"

# move files to trash dir.
move_files() {
    for f in $TARGET; do
	# remove path from filename.
	filename=${f##*/}
	mv "$f" "$T_DIR/$(stat --frintf %i $f)_$filename" && \
	    realpath $f >> $HISTORY_PATH && \
	    echo "[ok] $filename moved to trash" \
		|| echo "[error!] $filename"
    done
}

if [ -n "$TARGET" ]; then
    move_files
else
    echo "falta completar el RESTORE!"
fi


