#!/bin/sh

## wallpaper in .png or .xbm. External variable file dependent colors ##
# !ImageMagick. Version 7 and later change the "convert" command to "magick".
# xl_bitwall.sh by @root_informatica.

INPUT=$1
# bitmap path.
B_PATH="$HOME/.bitmap.xbm"
# screen resolution.
RES=$(sed 's/,/x/' < /sys/class/graphics/fb0/virtual_size)

# put the existing bitmap in root-window.
set_bitmap() {
    [ -f "$B_PATH" ] && \
	xsetroot -fg "#404040" -bg "#101010" -bitmap $B_PATH
}

# create bitmap from an image.
make_bitmap() {
    [ -f "$INPUT" ] && \
	magick $INPUT -colorspace Gray \
		       -posterize 3 \
		       -ordered-dither checks \
		       -negate \
		       -resize "!$RES" $B_PATH
}

# let's proceed
[ -n "$INPUT" ] && \
    make_bitmap

set_bitmap

