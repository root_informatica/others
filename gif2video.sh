#!/bin/sh

## convert gif files to mp4. Ideal for terminal output capture files such as recterm ##
# gif2video.sh by @root_informatica.

# input file.
TARGET="$1"

# check if input file exist.
[ -f "$TARGET" ] && \
    ext="${TARGET##*.}" \
       || exit 0

# check if input file is a gif.
[ "$ext" = "gif" ] && \
    ffmpeg -i $TARGET \
	   -movflags faststart \
	   -pix_fmt yuv420p \
	   -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" gif2video.mp4 \
	|| echo "the file is not a .gif"

