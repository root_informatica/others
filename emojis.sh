#!/bin/sh

# some fun with ascii emojis ##
# emojis.sh by @root_informatica.

# emojis path.
E_PATH="$HOME/.emojis"

# check if runing inside a terminal.
if [ -t 0 ]; then
    # pull up the file with fzfmenu and store in a termporary location.
    MENU=fzfmenu.sh
    $MENU < $E_PATH | cut -d ' ' -f 3- > /tmp/emojis
        
else
    # pull up the file with xmenu.sh and send to clipboard.
    MENU=xmenu.sh
    $MENU < $E_PATH | cut -d ' ' -f 3- | xclip -sel c
fi

## .emojis file example:
# acid  ⊂(◉‿◉)つ
# afraid  (ㆆ_ㆆ)
# angel  ☜(⌒▽⌒)☞
# angry x (•`_´•)
