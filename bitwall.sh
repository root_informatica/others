#!/bin/sh

## select and set bitmaps as desktop background ##
# bitwall.sh by @root_informatica

# bitmaps dir.
B_DIR="$HOME/git/bitmaps"
# bitmap path.
B_PATH="$HOME/.bitmap.xbm"
# foreground and background colors.
FG="#404040"
BG="#101010"

# define the menu according to the environment.
[ -t 0 ] && MENU="fzy" \
           || MENU="xmenu.sh"

target=$(find $B_DIR -type f | grep .xbm | cut -d '/' -f 6- | $MENU)
[ -n "$target" ] && \
    cp $B_DIR/$target $B_PATH && \
    xsetroot -fg "$FG" -bg "$BG" -bitmap $B_PATH \
	|| exit 0

# remove path.
# awk -F/ '{print $NF}'

# short path.
# cut -d '/' -f 6-
