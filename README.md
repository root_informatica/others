# scripts
other personal scripts

**ATTENTION!** This repo is just a snapshot in time of some scripts from my home directory. I don't plan to update these files very often and if it does so, it will not be in an organized manner nor will there be a continuation over time for any of the particular scripts. They remain in this visible repo not so much to be used as tools, but rather to get ideas from them.

**content:**

[adblocklist.sh] = download adblock list for luakit. 

[bitmap.sh] = select and set bitmaps as desktop background. 

[browser.sh] = open a minimal browser in different scenarios. 

[dmagnet.sh] = convert magnet to torrent? (It's not mine. I found it somewhere). 

[dkr.sh] = start docker and containerd services whith runit. 

[dzpanel.sh] = some info width dzen2. 

[emax.sh] = Emacs launcher. check if we are running inside a terminal emulator or not. 

[emojis.sh] = some fun with ascii emojis. 

[ffurlplayer.sh] = play urls width ffplay. 

[fzfmenu.sh] = fzf menu for non-X sessions. 

[fzfmp.sh] = play music with mplayer and fzf. 

[fzfpreview] = utility for previewing content in fzf. 

[luabrowser.sh] = overly sophisticated launcher for luakit. 

[lukylove.sh] = try your luck in love. 

[mail_sync.sh] = update mails with mbsync. 

[musicplayer.sh] = play music with mplayer. 

[nextloud_sync.sh] = sync with nextcloud. 

[nextcurl.sh] = manipulate files to and from nextcloud. 

[note.sh] = dummy notes. 

[picker.sh] = crappy script to browse and open files in different scenarios. 

[play.sh] = play video with or without X server (Xorg). 

[podcastplayer.sh] = podcast player. 

[power.sh] = Monitor battery status and do a few things accordingly. 

[proc.sh] = simple proces manager for X and non-X sessions. 

[qr.sh] = create qr codes from any url in terminal with image file option as output. 

[radioplayer.sh] = play online radio  with mplayer. 

[read.sh] = read pdf's with or without X server (Xorg). 

[record.sh] = capture desktop content in video. 

[roach.sh] =  put some crawlers on your desk for a change. 

[rooter.sh] = a kind of Swiss army knife. 

[rootfetch.sh] = this fetch was born from the search for a simpler way to get system info. 

[screenshot.sh] = images and bragging with your pretty desktop. 

[session.sh] = manage session and some more things. 

[sfetch.sh] = like rootfetch but with less colo. 

[spacekiller.sh] = replace spaces in file and directory names with a "_". 

[status.sh] = simple status. 

[termgeometry.sh] = get the dimensions and location of a terminal in root-window. 

[terminalsaver.sh] = stupid gif as terminal saver. 

[time.sh] = adjust computer clock based on internet time. 

[urlizer.sh] = upload files and share with 0x0.st in x or non-X sessions. 

[usblink.sh] =  mount and umount partitions a little less than "by hand" (never finished). 

[vol.sh] = vol level, alxamixer, dzen2 notifications. 

[wallpaper.sh] = wallpaper in .png or .xbm. 

[watch.sh] = view images with or without X server (Xorg). 

[windowblur.sh] = blur the content of a window to hide it. 

[windowsaver.sh] = put some image witch nsxiv into the windows. 

[xlauncher.sh] = very simple and sexiest graphic launcher made with your beloved plus fzy terminal. 

[xmenu.sh] = very simple and sexiest graphic menu made with your beloved plus fzy terminal. 

[xmpv.sh] = launch mpv together with a graphic session. 

[xselgo.sh] = open web browser with the content of x selection. 

[xtoclip.sh] = xtoclip. 

[youtube_sync.sh] = Download new content from your favorite YouTube channels.


