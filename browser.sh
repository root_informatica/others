#!/bin/sh

## open a minimal browser in different scenarios ##
# browser.sh by @root_informatica.

FLAG=$1
# tor pid.
T_PID=$(pgrep -x tor)
# bookmarks path.
B_PATH="$HOME/.bookmarks"

# discriminate depending on whether or not there is a graphical environment.
if [ -t 0 ]; then
    MENU="fzy"
    # default web browser command.
    WB_COMMAND="links"
    # tor web browser command.
    TWB_COMMAND="links -socks-proxy 127.0.0.1:9050 -only-proxies 1"
else
    MENU="xmenu.sh"
    WB_COMMAND="links -g"
    TWB_COMMAND="links -g -socks-proxy 127.0.0.1:9050 -only-proxies 1"
fi

# define the browser based on the flag.
case $FLAG in
    -t)    # torsock browser (verify that the tor service is running).
	if [ -n "$T_PID" ]; then
	    browser=$TWB_COMMAND
	else
	    exit 0
	fi
	;;
    *)    # default browser.
         	browser=$WB_COMMAND
	;;
esac

# chose website and grep it in ~/.bookmarks.
# these two steps respond to aesthetic motivations.
INPUT=$(awk '{print $1}' < $B_PATH | $MENU)
URL=$(cat $B_PATH | grep ^$INPUT | cut -d ' ' -f 2)

# compare input, open input, open input url, search input o just exit.
if [ -n "$INPUT" ]; then
    if [ -n "$URL" ]; then
	$browser $URL

    else
	echo "$INPUT" | grep -Eq "^https://|^http://|^www." && $browser $INPUT \
		|| $browser "https://search.disroot.org/?q=$INPUT"
    fi

else
    exit 0
fi

## .bookmarks example: ##
## website_1 https://website_1_example
## website_2 https://website_2_example

## search example: ##
## "https://duckduckgo.com/?q=$INPUT"

