#!/bin/sh

## simple proces manager for X and non-X sessions ##
# proc.sh by @root_informatica 

[ -t 0 ] && MENU="fzy" \
	|| MENU="xmenu.sh"

# choose the signal to send.
SIGNAL=$(echo -e "pause\ncontinue\nkill" | $MENU)
if [ "$SIGNAL" ]; then
    # choose the proces.
    proc=$(pgrep -a -u $USER | cut -c 1-100 | $MENU | cut -d ' ' -f 1)
    if [ "$proc" ]; then
        case $SIGNAL in
            pause)    # pause process.
	        kill -STOP $proc
	        ;;
            continue)    # resume process.
	        kill -CONT $proc
	        ;;
            kill)    # kill process.
	        kill $proc
	        ;;
        esac
    fi
fi
