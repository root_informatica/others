#!/bin/sh

## vol level, alxamixer, dzen2 notifications ##
# vol.sh by @root_informatica.

INPUT=$1

# usage
usage() {
    cat <<EOF
usage:
vol.sh [-d, -i, 0 t0 100]
-d) decrease volume 10%.
-i) increase volume 10%.
-t) togle mute.
0 to 100) move the volume between values 0% t0 100%.
EOF
}

case $INPUT in
    -d)
	amixer sset Master 10%- > /dev/null 2>&1
	;;
    -i)
	amixer sset  Master 10%+ > /dev/null 2>&1
	;;
    -t)
	amixer set Master toggle > /dev/null 2>&1
	;;
    [0-9]|[1-9][0-9]|100)
	amixer sset Master $INPUT% > /dev/null 2>&1
	;;
    *)
	usage
	;;
esac

# dzen2/terminal notification stuff.
DZ_PID=$(pgrep dzen)
[ -n "$DISPLAY" ] && dzpanel.sh -v \
	|| echo "volume level: $(amixer sget Master | awk -F "[][]" '/Playback/ {print $2}' | tr -d '\n')"
