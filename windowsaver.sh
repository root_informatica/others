#!/bin/sh

## put some image witch nsxiv into the windows ##
# windowsaver.sh by @root_informatica.

# gif path.
GIF_PATH="$HOME/.gif"
# window target. (wmutils)
W_TARGET=$(pfw)
# window geometry. (wmutils)
GEO=$(wattr wh $(pfw) | sed 's/ /x/g')

# saver it.
nsxiv -ab -z 120 -g $GEO -e $W_TARGET $GIF_PATH


#### WID and GEO widh "xdotool" ####
# WID=$(xdotool getwindowfocus)
# GEO=$(xdotool getwindowfocus getwindowgeometry | awk -F " " '/Geometry/ {print $2}'
