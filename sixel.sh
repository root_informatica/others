#!/bin/sh

## small script to preview images in terminals that support sixel graphics ##
# it depends on ImageMagick.
# sixel.sh by @root_informatica.

INPUT=$1

if [ -d "$INPUT" ]; then
    files=$(find "$INPUT" -maxdepth 1 -type f | grep -E ".png|.jpg|.jpeg|.xbm")
    [ -n "$files" ] && \
	target=$(echo "$files" | fzy) \
	      || exit 0
else
    target="$INPUT"
fi

magick $target -geometry 800x480 sixel:-
