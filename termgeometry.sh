#!/bin/sh

## This script is useful only for terminals that adjust their size in relation to the size of the text and that define their geometry in columns and rows ##
# termgeometry.sh by @root_informatica.

## usage ##
# 1- Open a terminal, adjust its size however you want.
# 2- Run the script and it will center it and output its geometry to stdout

FW=$(pfw)                      # focused window 
RW=$(wattr w $(lsw -r))        # root width
RH=$(wattr h $(lsw -r))        # root height
FWW=$(wattr w $FW)             # focused window width
FWH=$(wattr h $FW)             # focused window height 
CENTER_X=$(((RW - FWW) / 2))    # center xcalcuate
CENTER_Y=$(((RH - FWH) / 2))    # center y calculate

# center the window
wtp $CENTER_X $CENTER_Y $FWW $FWH $FW

columns=$(tput cols)    # terminal columns
rows=$(tput lines)      # terminal rows
x_off=$(wattr x $FW)     # terminal xoff
y_off=$(wattr y $FW)     # terminal yoff

# print the output
printf "

 -g ${columns}x${rows}+${x_off}+${y_off}

 "
