#!/bin/sh

## upload files and share with 0x0.st in x or non-X sessions ##
# urlizer.sh by @root_informatica.

INPUT=$1

# path definition.
if [ -n "$INPUT" ]; then
    [ -f "$INPUT" ] && \
	target="$INPUT" \
	    || exit 0
else

    # menu variable depending on the environment
    [ -t 0 ] && MENU="fzy" \
	    || MENU="xmenu.sh"
    
    # filter definitions.
    OPT=$(echo -e "documentos\nimágenes\nmúsica\nscripts\nvídeos" | $MENU)
    case $OPT in
	documentos)
	    filter=".doc|.pdf|.txt"
	    ;;
	imágenes)
	    filter=".jpg|.jpeg|.png|.webp|.xbm"
	    ;;
	música)
	    filter=".flac|.mp3|.ogg|.opus|.wav|.webm"
	    ;;
	scripts)
	    filter=".bash|.pl|.py|.sh"
	    ;;
	vídeos)
	    filter=".avi|.mkv|.mp4|"
	    ;;
    esac
    
    [ -n "$filter" ] && \
	target=$(find $HOME -type f | grep -E "$filter" | $MENU) \
		|| exit 0
fi

# urlize!.
if [ -n "$target" ]; then
    link=$(curl  -F "file=@$target" -F "secret=" https://0x0.st)
    # chkeck if link is ok.
    ok_link="https://0x0.st/*"
    # if display or not.
    case $link in
	$ok_link)
	    if [ -n "$DISPLAY" ]; then
		# copy link to clipboard.
		echo "$link" | xclip
		# make it available to other programs
		xclip -o | xclip -sel c
	    else
		# upload the file and save the path in /tmp
		echo "$link" > /tmp/urlizer
	    fi
	    ;;
	*) exit 1
	   ;;
    esac
fi

