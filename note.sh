#!/bin/sh

## dummy notes ##
# note.sh by @root_informaitca

# extension.
EXT=".txt"
# note title.
TITLE="$1"
# note dir.
N_DIR="$HOME/nextcloud/notas/"
NOTE="$N_DIR$TITLE$EXT"

usage() {
    cat<<EOF
usage:
note.sh <title>
:erease) erease note. 
:dline) delete line/item.
:order) sort items.
:quit) save and exit.
EOF
}

# delete line.
d_line() {
    line_number=$(cat -n $NOTE | fzy | cut -c 6)
    [ -n "$line_number" ] && \
	sed -i "${line_number}d" $NOTE
}

# erease note.
erease() {
    : > $NOTE
}

# preview.
preview() {
    clear
    [ -f "$NOTE" ] && \
	cat -n $NOTE
}

# draw a simple bar.
bar() {
    printf "
\033[1;7;37m[ compose  $TITLE$EXT ]\033[3;7;30m -- :editor -- :erease -- :dline -- :quit -- \033[0m
"
}

if [ -n "$TITLE" ]; then
    preview
    bar
    while read IN; do
	case $IN in
	    :editor)
		$EDITOR $NOTE
		;;
	    :erease)
		erease
		;;
	    :dline)
		d_line
		;;
	    :quit)
		exit 0
		;;
	    *)
		echo "$IN" >> $NOTE
		;;
	esac
	preview
	bar
    done

else
    usage
fi
