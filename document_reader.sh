#!/bin/sh

## read pdf's with or without X server (Xorg) ##
# document_reader.sh by @root_informatica.

TARGET=$1

# check display.
[ -n "$DISPLAY" ] && zathura "$TARGET" \
	|| fbpdf "$TARGET"

