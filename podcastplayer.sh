#!/bin/sh

## podcast player ##
# podcastplayer.sh by @root_informatica.

# podcast dir path.
P_DIR="$HOME/download/ytpd"
# last podcast played path. (in case of fire)
LP_PATH="/tmp/lastpodcast.ogg"
# menu.
MENU=fzy

# check if the directory is empty or not
[ "$(ls -A $P_DIR)" ] && \
    # select the target file.
    podcast=$(find $P_DIR -type f | grep .ogg | awk -F/ '{print $NF}' | $MENU) \
	|| exit 0

# if a podcast has been given.
if [ -n "$podcast" ]; then
    target="$P_DIR/$podcast"
    # play it and then move to LP.
    mpv "$target"
    mv "$target" "$LP_PATH"
fi
