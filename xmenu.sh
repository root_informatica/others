#!/bin/sh

## very simple and sexiest graphic menu made with your beloved plus fzy terminal ##
# xmenu.sh by @root_informatica.

TERMINAL="xterm"
GEOMETRY="63x13+323+266"

$TERMINAL  -name "xmenu" \
	   -g $GEOMETRY \
	   -e "fzy < /proc/$$/fd/0 > /proc/$$/fd/1"

