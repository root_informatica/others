#!/bin/sh

## play music with mplayer ##
# fzfmp.sh by @root_informatica.

# music dir.
M_DIR="$HOME/Música/"
# playlist path.
P_LIST="/tmp/playlist"
# make a simple menu.


usage() {
    cat<<EOF
usage:
musicplayer.sh [ albun, song, delete, play, quit ]
album) add albums to the playlist.
song) add individual song/s to the playlist.
delete) delete playlist.
play) play it.
quit) exit.
use < and > for move backward and forwards in the playlist.
EOF
}

# add albums
add_album() {
    for song in $(find $M_DIR -type d | fzfmenu.sh); do
	find $song -maxdepth 1 -type f |
	    grep -E ".mp3|.opus|.flac|.webm|.wav" >> $P_LIST
    done
}

# add individual songs
add_song() {
    find $M_DIR -type f |
            grep -E ".mp3|.opus|.flac|.webm|.wav" |
            fzfmenu.sh >> $P_LIST
}

# delete playlist
delete_p_list() {
    if [ -f $P_LIST ]; then
	rm $P_LIST
    fi
}

# play it
play() {
    mpv --no-video --playlist=$P_LIST
}

error() {
    printf '%s\n' "WRONG ENTRY!"
    sleep 2
}

while true; do
    MENU=$(echo -e "album\nsong\ndelete\nplay\nview\nquit\nhelp" |
	       fzf --no-sort -m --preview="$(cat $P_LIST | cut -d '/' -f6-7)")
    case $MENU in
	album)
	    add_album
	    ;;
	song)
	    add_song
	    ;;
	play)
	    play
	    ;;
	delete)
	    delete_p_list
	    ;;
	quit)
	    break
	    ;;
	help)
	    usage
	    break
	    ;;
	*)
	    error
	    ;;
    esac
done
