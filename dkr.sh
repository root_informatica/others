#!/bin/sh

## start docker and containerd services whith runit ##
# I did it so I wouldn't have the services running all the time and could start them on demand.
# It is necessary to have a file called "down" in the /etc/sv/<service> directory
# dkr.sh by @root_informatica

FLAG=$1
# docker pid.
D_PID=$(pgrep -x docker)
# Containerd pid.
C_PID=$(pgrep -x containerd)

usage() {
	cat<<EOF
up) start docker services.
down) stop docker services.
EOF
}

# start docker and containerd.
up() {
    [ -n "$D_PID" ] && \
	echo "docker is already running!" \
	     ||	sv start docker
    [ -n "$C_PID" ] && \
	echo "containerd is already running!" \
	     || sv start containerd
}

# stop docker and containerd.
down() {
    [ -n "$D_PID"  ] && \
	sv stop docker \
	    || echo "docker is already stoped!"
    [ -n "$C_PID" ] && \
	sv stop containerd \
	   || echo "containerd is already stoped!"
}

case $FLAG in
    up)
	up
	;;
    down)
	down
	;;
    *)
	usage
	;;
esac
