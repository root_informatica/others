#!/bin/sh

## very simple and sexiest graphic launcher made with your beloved plus fzy terminal ##
# xlauncher.sh by @root_informatica.

TERMINAL="xterm"
GEOMETRY="63x13+323+266"

 $TERMINAL -name 'xlauncher' \
	   -g $GEOMETRY \
	   -e 'cmd=$({ IFS=:; set -f; find -L $PATH -maxdepth 1 -type f -perm -100 -print; } \
	      	       | sed 's!.*/!!' \
		       | sort -u \
		       | grep -v fzy \
		       | fzy); \
	       setsid -f $cmd'
