#!/bin/sh

## put some crawlers on your desk for a change ##
# depends on: xroach.
# shuf is not POSIX.
# roach.sh by @root_informatica.

# xroach pid.
XR_PID=$(pgrep -x xroach)

if [ -n "$XR_PID" ]; then
    pkill xroach
else
    # random numbers.
    r1=$(shuf -i 1-19 -n 1)
    s1=$(shuf -i 1-19 -n 1)
    # random values.
    r2=$(( 20 - r1 ))
    s2=$(( 20 - s1 ))
    # make the magic.
    xroach -squish -rc "#420807" -roaches $r1 -speed $s1 -rgc yellowgreen &
    xroach -squish -rc "#60220e" -roaches $r2 -speed $s2 -rgc yellowgreen &
fi
