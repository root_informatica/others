#!/bin/sh

## Monitor battery status and do a few things accordingly ##
## It's not a very generic script, let's say ##
# power.sh by @root_informatica.

# battery status and level.
read -r STATUS < /sys/class/power_supply/BAT0/status
read -r LEVEL < /sys/class/power_supply/BAT0/capacity

# alarm function. (depends of wms_batlarm.sh)
alarm() {
    if [ -n "$DISPLAY" ] && [ -z "$(pgrep wms_batlarm.sh)" ]; then
	wms_batlarm.sh &
    fi
}

# hibernate function (void-linux)
hibernate() {
    doas zzz -Z
}

# if status and level match send alarm 
if [ $STATUS = "Discharging" ] && [ $LEVEL -le "50" ]; then 
    alarm
fi

# if status and level match, hibernate
if [ $STATUS = "Discharging" ] &&  [ $LEVEL -le "30" ]; then
    hibernate
fi
